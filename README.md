# Demo Tank v0.0.0

This is a small demonstration of a tank as a test task for 'Lesta Games' [C++ developer](https://startlesta.by/ru/courses/cplusdeveloper/) courses.

## How to build
1. Clone this repository on local machine  
  ```git clone --depth 1 https://gitlab.com/Chargebrains/demo_tank.git```
2. Go to the project root   
  ```cd ./demo_tank```
3. Build the project  
  3.1 ```mkdir build```  
  3.2 ```cd build```  
  3.3 ```cmake ..```  
  3.4 ```cmake –-build .```  
4. Run the game from root  
  4.1 ```cd ..```  
  4.2 ```./build/game```
5. Enjoy

## Game Controls

```W,A,S,D``` - Standard moving (up,left,down,right)   
```Space``` - Fire   
```Esc``` - Exit game  
