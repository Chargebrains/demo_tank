#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/NonCopyable.hpp>

#include "game_object.h"
#include "texture_holder.h"

#include <chrono>
#include <list>
#include <memory>
#include <unordered_map>
#include <vector>

class tank_demo_app final : sf::NonCopyable
{
public:
    tank_demo_app();

    int run(); // return exit code [EXIT_SUCCESS or EXIT_FAILURE]

private:
    bool initialize();
    int  game_loop();

    void process_events();
    void update(const std::chrono::milliseconds frame_delta);
    void render();

    void process_player_input();
    void process_bullet_creation();

    void check_bullets_lifetime(const std::chrono::milliseconds frame_delta);

    static size_t get_unique_id();

    sf::RenderWindow window_;
    texture_holder   textures_;

    std::list<std::unique_ptr<game_object>> bullets_;
    std::unordered_map<size_t, size_t>      bullets_lifetime_; // [key: id, value: milliseconds timer]
    std::unique_ptr<game_object>            players_tank_;
    std::chrono::milliseconds               time_since_last_shot_;
    std::unique_ptr<game_object>            background_;
};
