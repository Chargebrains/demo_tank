#pragma once

#include <cmath>
#include <limits>
#include <type_traits>

namespace utils::math
{
/// Small function for comparison floating point numbers
template <class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type almost_equal(T x, T y, const int ulp)
{
    return std::fabs(x - y) <= std::numeric_limits<T>::epsilon() * std::fabs(x + y) * ulp ||
           std::fabs(x - y) < std::numeric_limits<T>::min();
}

} // namespace utils::math