#pragma once

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include <chrono>

enum class movement_direction
{
    none,
    up,
    left,
    right,
    down
};

enum class object_type
{
    tank,
    bullet,
    background
};

struct transform_info
{
    sf::Vector2f position{ 0.f, 0.f };
    sf::Vector2f scale{ 1.f, 1.f };
    float        rotation{ 0.f };
};

class game_object final : public sf::Drawable
{
public:
    game_object(const object_type type, const size_t id, const sf::Texture& texture, const transform_info& transform);
    ~game_object() override = default;

    void update(const std::chrono::milliseconds frame_delta);
    void draw(sf::RenderTarget& target, sf::RenderStates states) const final;

    void set_movement_direction(const movement_direction direction) { direction_ = direction; }

    sf::Vector2f get_position() const { return transform_.position; }
    float        get_rotation() const { return transform_.rotation; }

    size_t get_id() const { return id_; }

private:
    float get_speed_by_type();

    transform_info     transform_;
    sf::Sprite         sprite_;
    movement_direction direction_;
    object_type        type_;
    size_t             id_; // unique identifier for each class instance
};

// some helper functions
movement_direction rotation_to_direction(const float rotation);
sf::Vector2f       offset_by_direction(const movement_direction direction, const float offset);
