#include "tank_demo_app.h"
#include "tank_demo_config.h"

#include <SFML/Window/Event.hpp>
#include <SFML/Window/Keyboard.hpp>

#include <chrono>
#include <iostream>
#include <memory>
#include <thread>

tank_demo_app::tank_demo_app()
    : window_{}
    , textures_{}
    , bullets_{}
    , bullets_lifetime_{}
    , players_tank_{}
    , time_since_last_shot_{ std::chrono::milliseconds::zero() }
    , background_{}

{
}

int tank_demo_app::run()
{
    if (!initialize())
    {
        return EXIT_FAILURE;
    }

    return game_loop();
}

bool tank_demo_app::initialize()
{
    // init window
    {
        window_.create(sf::VideoMode(config::window::width, config::window::height), config::window::title);
    }

    // load all needed textures
    {
        textures_.load(textures_id::tank, config::assets::tank);
        textures_.load(textures_id::bullet, config::assets::bullet);
        textures_.load(textures_id::background, config::assets::background);
    }

    // init background image
    {
        namespace config = config::game_objects::background;
        transform_info t{ .position{ config::position_x, config::position_y },
                          .scale{ config::scale_x, config::scale_y },
                          .rotation = config::rotation };
        background_ = std::make_unique<game_object>(
            object_type::background, get_unique_id(), textures_.get(textures_id::background), t);
    }

    // init player's tank
    {
        namespace config = config::game_objects::tank;
        transform_info t{ .position{ config::position_x, config::position_y },
                          .scale{ config::scale_x, config::scale_y },
                          .rotation = config::rotation };
        players_tank_ =
            std::make_unique<game_object>(object_type::tank, get_unique_id(), textures_.get(textures_id::tank), t);
    }

    return true;
}

int tank_demo_app::game_loop()
{
    using clock_timer = std::chrono::high_resolution_clock;
    using nano_sec    = std::chrono::nanoseconds;
    using milli_sec   = std::chrono::milliseconds;
    using time_point  = std::chrono::time_point<clock_timer, nano_sec>;

    time_point start{ clock_timer::now() };

    while (window_.isOpen())
    {
        const time_point end_last_frame{ clock_timer::now() };

        process_events();

        const milli_sec frame_delta{ std::chrono::duration_cast<milli_sec>(end_last_frame - start) };
        if (frame_delta.count() < config::window::ms_per_frame)
        {
            std::this_thread::yield();
            continue;
        }

        update(frame_delta);
        render();

        start = end_last_frame;
    }

    return EXIT_SUCCESS;
}

void tank_demo_app::process_events()
{
    sf::Event event{};
    while (window_.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            window_.close();
        }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
    {
        window_.close();
    }

    process_player_input();
}

void tank_demo_app::update(const std::chrono::milliseconds frame_delta)
{
    time_since_last_shot_ += frame_delta;

    std::for_each(bullets_.begin(),
                  bullets_.end(),
                  [&](auto& obj)
                  {
                      if (!obj)
                      {
                          throw std::runtime_error{ "Error: bullet is nullptr." };
                      }
                      obj->update(frame_delta);
                  });

    check_bullets_lifetime(frame_delta);

    if (!players_tank_)
    {
        throw std::runtime_error{ "Error: player's tank is nullptr." };
    }
    players_tank_->update(frame_delta);
}

void tank_demo_app::render()
{
    window_.clear();

    if (!players_tank_)
    {
        throw std::runtime_error{ "Error: background is nullptr." };
    }
    window_.draw(*background_);

    if (!players_tank_)
    {
        throw std::runtime_error{ "Error: player's tank is nullptr." };
    }
    window_.draw(*players_tank_);

    std::for_each(bullets_.begin(),
                  bullets_.end(),
                  [&](const auto& obj)
                  {
                      if (!obj)
                      {
                          throw std::runtime_error{ "Error: bullet is nullptr." };
                      }
                      window_.draw(*obj.get());
                  });

    window_.display();
}
void tank_demo_app::process_player_input()
{
    movement_direction d{ movement_direction::none };

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
    {
        d = movement_direction::up;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        d = movement_direction::left;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
        d = movement_direction::down;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        d = movement_direction::right;
    }

    if (!players_tank_)
    {
        throw std::runtime_error{ "Error: player's tank is nullptr." };
    }
    players_tank_->set_movement_direction(d);

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
    {
        process_bullet_creation();
    }
}
size_t tank_demo_app::get_unique_id()
{
    // Generate unique id for each game object.
    // I hope that size_t unique values is enough for demo :)
    // Of course better solution it returns unused ids, but i some lazy do it for demo app
    static size_t unique_id_counter{ 0 };
    return ++unique_id_counter;
}

void tank_demo_app::process_bullet_creation()
{
    namespace config = config::game_objects::bullet;
    if (time_since_last_shot_.count() <= config::bullet_reloading)
    {
        return;
    }

    { // bullet creation
        transform_info t{};
        t.rotation = players_tank_->get_rotation();

        const movement_direction bullet_direction{ rotation_to_direction(t.rotation) };
        t.position = players_tank_->get_position() + offset_by_direction(bullet_direction, config::offset_from_player);

        auto b{ std::make_unique<game_object>(
            object_type::bullet, get_unique_id(), textures_.get(textures_id::bullet), t) };
        b->set_movement_direction(bullet_direction);

        bullets_lifetime_[b->get_id()] = 0;
        bullets_.push_back(std::move(b));
    }
    // made a shot so time to reload
    time_since_last_shot_ = std::chrono::milliseconds::zero();
}
void tank_demo_app::check_bullets_lifetime(const std::chrono::milliseconds frame_delta)
{
    // delete old bullets by timer
    const auto frame_delta_count{ frame_delta.count() };
    for (auto it = bullets_.begin(); it != bullets_.end();)
    {
        if (!*it)
        {
            throw std::runtime_error{ "Error: bullet is nullptr." };
        }

        const auto bullet_id{ (*it)->get_id() };

        if (!bullets_lifetime_.contains(bullet_id))
        {
            throw std::runtime_error{ "Error: bullet without lifetime data. Check bullet creation" };
        }

        auto& bullet_lifetime{ bullets_lifetime_[bullet_id] };
        bullet_lifetime += frame_delta_count;

        if (bullet_lifetime >= config::game_objects::bullet::time_to_self_destruct)
        {
            it = bullets_.erase(it);
            bullets_lifetime_.erase(bullet_id);
        }
        else
        {
            ++it;
        }
    }
}
