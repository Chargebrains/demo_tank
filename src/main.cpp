#include "tank_demo_app.h"

#include <iostream>

int main()
{
    try
    {
        tank_demo_app tank_demo;
        return tank_demo.run();
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
    catch (...)
    {
        std::cerr << "Error: something went wrong!\n"
                     "I'm sorry, I don't know anything else. Something is really bad there."
                  << std::endl;
    }

    return EXIT_FAILURE;
}
