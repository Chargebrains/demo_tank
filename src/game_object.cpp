#include "game_object.h"
#include "tank_demo_config.h"
#include "utils.h"

#include <sstream>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Texture.hpp>

game_object::game_object(const object_type     type,
                         const size_t          id,
                         const sf::Texture&    texture,
                         const transform_info& transform)
    : transform_(transform)
    , sprite_{}
    , direction_(movement_direction::none)
    , type_{ type }
    , id_{ id }
{
    // init object's sprite with origin in center
    sprite_.setTexture(texture);
    const auto texture_size{ texture.getSize() };
    sprite_.setOrigin(texture_size.x / 2, texture_size.y / 2);
}

void game_object::update(const std::chrono::milliseconds frame_delta)
{
    // update rotation and position by direction
    const float offset{ get_speed_by_type() * frame_delta.count() * 0.001f };
    switch (direction_)
    {
        case movement_direction::up:
            transform_.rotation = 0.f;
            transform_.position.y -= offset;
            break;
        case movement_direction::left:
            transform_.rotation = 270.f;
            transform_.position.x -= offset;
            break;
        case movement_direction::right:
            transform_.rotation = 90.f;
            transform_.position.x += offset;
            break;
        case movement_direction::down:
            transform_.rotation = 180.f;
            transform_.position.y += offset;
            break;
        case movement_direction::none: /* without change */
            break;
        default:
            throw std::runtime_error{ "Error: Unknown movement direction." };
    }
}

void game_object::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    sf::Transformable t{};
    t.scale(transform_.scale);
    t.rotate(transform_.rotation);
    t.move(transform_.position);

    states.transform *= t.getTransform();
    target.draw(sprite_, states);
}

float game_object::get_speed_by_type()
{
    switch (type_)
    {
        case object_type::bullet:
            return config::game_objects::bullet::speed;
        case object_type::tank:
            return config::game_objects::tank::speed;
        case object_type::background:
            return 0.f;
        default:
            throw std::runtime_error{ "Error: Unknown object type." };
    }
}

movement_direction rotation_to_direction(const float rotation)
{
    namespace um = utils::math;
    if (um::almost_equal(rotation, 0.f, 3))
    {
        return movement_direction::up;
    }
    else if (um::almost_equal(rotation, 90.f, 3))
    {
        return movement_direction::right;
    }
    else if (um::almost_equal(rotation, 180.f, 3))
    {
        return movement_direction::down;
    }
    else if (um::almost_equal(rotation, 270.f, 3))
    {
        return movement_direction::left;
    }

    std::ostringstream ss;
    ss << "Error: Unknown rotation. Rotation value is " << rotation;
    throw std::runtime_error{ ss.str() };
}

sf::Vector2f offset_by_direction(const movement_direction direction, const float offset)
{
    switch (direction)
    {
        case movement_direction::up:
            return sf::Vector2f{ 0.f, -offset };
        case movement_direction::left:
            return sf::Vector2f{ -offset, 0.f };
        case movement_direction::right:
            return sf::Vector2f{ offset, 0.f };
        case movement_direction::down:
            return sf::Vector2f{ 0.f, offset };
        case movement_direction::none:
            return sf::Vector2f{ 0.f, 0.f }; // something strange
        default:
            throw std::runtime_error{ "Error: Unknown movement direction." };
    }
}
