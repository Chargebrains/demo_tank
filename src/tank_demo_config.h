#pragma once

/// All game configurations
namespace config
{
namespace window
{
static constexpr unsigned int width{ 800u };
static constexpr unsigned int height{ 600u };
static constexpr char const*  title{ "Tank demo app v 0.0.0" };
static constexpr int          ms_per_frame{ 16 };
} // namespace window

namespace assets
{
static constexpr const char* tank{ "assets/tank.png" };
static constexpr const char* bullet{ "assets/bullet.png" };
static constexpr const char* background{ "assets/background.png" };
} // namespace assets

namespace game_objects
{
namespace background
{
static constexpr float position_x{ 400.f };
static constexpr float position_y{ 300.f };
static constexpr float scale_x{ 1.f };
static constexpr float scale_y{ 1.f };
static constexpr float rotation{ 0.f };

} // namespace background

namespace tank
{
static constexpr float position_x{ 400.f };
static constexpr float position_y{ 300.f };
static constexpr float scale_x{ 1.f };
static constexpr float scale_y{ 1.f };
static constexpr float rotation{ 0.f };
static constexpr float speed{ 100.f };
} // namespace tank

namespace bullet
{
static constexpr float  speed{ 400.f };
static constexpr size_t time_to_self_destruct{ 3000 }; // milliseconds
static constexpr int    bullet_reloading{ 500 };       // milliseconds
static constexpr float  offset_from_player{ 60.f };
} // namespace bullet
} // namespace game_objects
} // namespace config
