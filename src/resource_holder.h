#pragma once

#include <cassert>
#include <memory>
#include <stdexcept>
#include <string>
#include <unordered_map>

template <typename Resource, typename Identifier>
class resource_holder
{
public:
    void load(Identifier id, const std::string& filename);

    const Resource& get(Identifier id) const;

private:
    std::unordered_map<Identifier, std::unique_ptr<Resource>> resource_map_;
};

template <typename Resource, typename Identifier>
void resource_holder<Resource, Identifier>::load(Identifier id, const std::string& filename)
{
    std::unique_ptr<Resource> resource{ std::make_unique<Resource>() };

    if (!resource->loadFromFile(filename))
    {
        throw std::runtime_error{ "Error: Failed to load resource " + filename };
    }

    const bool inserted{ resource_map_.insert(std::make_pair(id, std::move(resource))).second };
    assert(inserted && "Assert: Resource is not inserted. It may have already been loaded before.");
}

template <typename Resource, typename Identifier>
const Resource& resource_holder<Resource, Identifier>::get(Identifier id) const
{
    const auto found{ resource_map_.find(id) };
    assert(found != resource_map_.end() && "Assert: Resource not found. Check That resource has been loaded");
    return *found->second;
}
