#pragma once

#include "resource_holder.h"

#include <SFML/Graphics/Texture.hpp>

enum class textures_id
{
    tank,
    bullet,
    background
};

using texture_holder = resource_holder<sf::Texture, textures_id>;
